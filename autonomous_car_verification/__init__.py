from gymnasium.envs.registration import register
register(
     id="AutonomousCar-v0",
     entry_point="autonomous_car_verification.simulator.Car:World",
)
