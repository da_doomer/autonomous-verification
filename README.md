# Compositional Autonomous Car Verification

This is a fork of [https://github.com/keyshor/autonomous_car_verification](https://github.com/keyshor/autonomous_car_verification).

The name of the environment is "AutonomousCar-v0".

Changes:
- Fixed bug in `step` function
- Updated dependencies
- Added `pyproject.toml` file
- Packaged as a Gymnasium env
